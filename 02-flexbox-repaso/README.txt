FlexBox : es un modelo de cajas flexibles, esto
signifca que las dimensiones de las cajas son
elásticas y pueden expandirse o encogerse.
En otras palabras, aunque se definan un alto o 
ancho, estos valores no siempre serán respetados por
el navegador al dibujar el documento.

flexbox es un sistema para la maquetacion flexible, orientado
una dimensión y agnóstico en cuanto a dirección

En este apartado se estará analizando las redimensiones por defecto y personalizaciones que uno puede codificar.

tester e inspiración 
    - https://www.madebymike.com.au/demos/flexbox-tester/
    - https://ed.team/blog/guia-definitiva-de-flexbox-2-flex-basis-flex-frow-flex-shrink


Trucos flexbox (solo algunos)


- si se le coloca flex-basis : 0, todos los
    flex-items creceran de la misma forma, obviamente
    con flex-grow:1 tambien