- Layout : Es la geometria que calcula el navegador para saber donde colocar cada elemento y que tanto separarlo de los demas
- flexbox es un modelo de layout en una sola linea, si bien tiene un main-axis y un cross-axis pero estos dos solo es para alineación (flex-end , flex-start,etc),
  pero para organizar(colocar ele elemento en un lugar en la pantalla) elementos solo se puede usar el main-axis
  nunca podemos usar el cross-axis y mucho menos los dos al mismo tiempo
- flexbox : permite organizar elementos a los largo en un solo eje (main-axis)
- css grid : permite construir layout através de dos ejes . (horizontal y vertical | inline y block)
- No importa la posicion de un elemento en el código HTML para construir el layout
- Al igual que flexbox se require una relación de padre (grid-container) a hijo (grid-item)

Terminología y elementos

- Elementos del DOM - grid-container - elemento al que se le aplicó display:grid (el container será bloque hacia afuera) - elemento al que se le aplicó display:inline-grid (el container será un elemento de linea hacia afuera) - grid-items - hijos directos - pseudoelementos ::beforey ::after - textos

- No existen en el DOM, el navegador los crea para posicionar y alinear
  estos elementos son concepturales no existen en el DOM , pero algunos navegadores cuentan con el grid inspector - grid tracks (las filas y columnas) - grid line (se encuentras a los lados de los tranck, izquierda y derecha para columnas, y arriba y abajo para las filas ) - grid cells : interseccion entre una fila y una columna - grid areas : cualquier area rectangular delimitado por 4 grid-lines

Crear Templates

- grid-template-columns
- grid-template-rows
- grid-gap
- repeat()
	- repeat(5,100px)
	- repeat(3,1fr)
- Unidad fr
- grid-areas
- posicionar items

Primer ejemplo

```pug
  .container
    - for(let i=1;i<=20;i++)
     .item= `Item ${i}`
```

```scss
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
.container {
  display: grid;
  /*grid-template-columns: 100px 100px 100px 100px;*/
  /*grid-template-columns: 10% 10% 10% 10%;*/
  /*grid-template-columns: 20% 80%; sin fr*/
  /*grid-template-columns: 20% calc(80% - 10px); sin fr*/
  /*grid-template-columns: 10% 50% 10% 10% calc(20% - 40px); sin fr*/
  /*grid-template-columns : 20% 10% calc(70% - 20px); se resta 20px por que existe dos gap de 10px*/
  grid-template-columns: 20% 1fr; // el fr es fraccion: 20% del conteiner y el resto
  grid-template-rows: 100px 100px 100px 100px 100px;
  grid-gap: 10px;
  // con ese codigo se genera un scroll horizontal
  // para quitarlo
  // grid-template-column : 20% calc(80% - 10px);
  // pero ese codigo es ineficiente ya que si el layout en tema de columnas cambia
  // el gap autmenta y ese aumento tendriamos que restar para que se borre el scroll
  // para ello existe el fr, este indica el espacio disposible que se encuentra en el container
}
.item {
  display: flex;
  text-align: center;
  justify-content: center;
  align-items: center;
  background-color: yellow;
  &:nth-child(even) {
    background-color: pink;
  }
}
```

Segundo Ejemplo

```pug
  .container
  - let i = 18
  - while (i--)
    .item: img(src="https://images.pexels.com/photos/4110226/pexels-photo-4110226.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")
```

````scss
.container {
  width : 80%;
  display: grid;
  grid-template-columns: repeat(6,1fr);
  grid-auto-rows: 120px;
  grid-gap: 10px;
  grid-auto-flow: row dense;
  /*
    [start] y [end] son las referencias a los grid-lines NO grid-cell
    grid-column : [start] / [end]
    grid-row : [start] / [end]

    ejemplo :
      grid-column : 1 / 5; que comienze en el grid-line 1 y termine en el grid-line 5 || vertical
      grid-row : 1 / 5; que comienze en el grid-line 1 y termine en el grid-line 5 || horizontal 
    contar los grid-lines es una tortura lo mas recomendable es contar los track (dependeiendo, podemos contar columnas o filas) con span
    ejemplo :
      grid-column : 1 /  span 5; que comienze en el grid-line 1 y que tome 5 columnas|| vertical
      grid-row : 1 / span 5; que comienze en el grid-line 1 y que tome 5 filas || horizontal

  */
  }

.item {
  font-size: 2em;
  background: yellow;
  display: flex;
  align-items: center;
  justify-content: center;
  
  &:first-child {
    grid-column: span 2;
  }
  
  &:nth-child(2) {
    grid-column: span 3;
    grid-row: span 2;
  }
  
  &:nth-child(5) {
    grid-column: span 2;
    grid-row: span 3;
  }
  
  &:nth-child(6) {
    grid-column: span 3;
  }
  
  &:nth-child(8) {
    grid-column: span 3;
    grid-row: span 3;
  }
  
  &:nth-child(9) {
    grid-row: span 4;
  }
  
  
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  
  &:nth-child(even) {
    background: pink;
  }
}
````

Grid-responsive

````scss
.container {
  width : 80%;
  display: grid;
  // auto-fit : auto ajuste , lo que hara es calcular la cantidad de elementos que pueden entrar (tarea del navegador) y en funcion de que ?
  // minmax : minimo y maximo tamaño, esta será la dependencia que como minimo una columna puede ser 120px y como máximo 1fr
  grid-template-columns: repeat(auto-fit,minmax(120px,1fr));
  grid-auto-rows: 120px;
  grid-gap: 10px;
}
.item {
  font-size: 2em;
  background: yellow;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
}
````

## GRID PLACEMENT

El algoritmo de css grid es ***GRID PLACEMENT***, este algoritmo es el encargado de asignar
a cada elemento un tamaño , el lugar y el flujo que le corresponde.
Esto puede ser implicito o explicito.
Cuando a un elemento con `display:grid` no se le asigna un `grid-template-columns` y `grid-template-row`
no ocurrirá nada , al momento de asignarle un valor a cualquiera de los dos , el algoritmo
asignara automatica el comportamiento del contrario, es decir si se le asignó `grid-template-columns:repeat(6,100px)` pero en ningun momnento se asigno un `grid-template-row` el algoritmo le asignará el tamaño correspondiente al contenido y viceversa.

- Grid implícito
  - si se le coloca mas grid-lines de las que se habia establecido
  - ````scss
      .container{
        display:grid;
        grid-template-columns : 100px 100px;
      }
      .item{
        &:nth-child(2){
          grid-column: 7 / 100;
        }
      }
    ````
  - como se puede observar el total de columnas que se establecieron fueron dos cada una de 100px
  pero a un hijo se le asigno que comienze en el grid-line de columna 7 y termine en el 100
  por lo que ahora ya no existen 2 sino 100
- Grid explícito
  - se respeta los grid-lines establecidos

Para asegurarnos que los grid implicitos cuenten con un apoyo
- grid-auto-rows : number
- grid-auto-columns : number
- cada nuevo track que se cree tomará esas medidas, cada nuevo tranck que no se encuentre  
  en los medidas establecidas por `grid-template-columns` y `grid-template-row`
- grid-auto-flow: Row(default),Column y dense = define el sentido del grid, parecido al flex-direction

- Grid dinámico

- minmax()
  - ````scss
      main {
          height: 100vh;
          display: grid;
          font-family: sans-serif;
          grid-gap: 10px;
          grid-template-columns:
              /*1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr*/

              /*repeat(8, 1fr)*/

              /* 8 columnas con un mínimo y un máximo (límite) */
              // esto significa que cuando se redusca lo minimo que tendra de width una columna es de 10 y máximo una fraccion
              // si el viewport se reduce a un valor inferior a 10px ejemplo 5px el width de las columna no dejaran de ser 10px y lo que ocurrira es
              // que se generara un scroll vertical
              /*repeat(8, minmax(10px, 1fr));*/
          
              /* 8 columnas con un patrón dos (30px y mínimo 10% y máximo flexible */
              // lo que significa es que primero tendra una columna de 30px luego el siguiente estará en un rago dependediendo del viewport
              // luego comenzara con 30px y luego otra vez el rango
              /*repeat(4, 30px minmax(10%, 1fr));*/

              /* 8 columnas con un mínimo marcado por el contenido y un máximo del 30% */
              // es decir que lo minimo que tendra el width de cada columna será el width minimo del contenido
              /*repeat(8, minmax(min-content, 30%));*/

              /* 8 columnas con un máximo y mínimo marcado por su contenido */
              // significa que como minimo será el minimo que el contenido se reduzca (igual que el anterior ejemplo) y
              // como máximo será el width del contenido el 100% del cotenido
              repeat(8, minmax(min-content, max-content));
            
              /*repeat(auto-fit,minmax(120px,1fr))*/
            ;
      }

      div {
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: goldenrod;
      }

    ````
- min-content
- max-content
- auto-fill
- auto-fit

````pug
  section.auto-fill
    - for(let i=1;i<=4;i++)
      div=`Columna ${i}`
  section.auto-fit
    - for(let i=1;i<=4;i++)
      div=`Columna ${i}`
````

````css
.auto-fill {
    /*
      lo que hace es es ajustarce al contenido y si hay mas espacio en el viewport creará 
      mas tracks y mientras menos espacio haya en el viewport los tracks se reduciran 
      hasta que solo haya una columna

      en este grid-container posee 4 grid-iems pero la propiedad auto-fill lo que hará es 
      crear mas tracks si el viewport lo soporta, así que si el viewport soporta mas de 4 
      columnas habrá mas de 4 columnas y si el viewport se reduce, tambien las columnas
    */
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
}

.auto-fit {
    /*
      lo que hace esto es que calculara el total de columnas dependiendo del numero de hijos y si soporta en el 
      viewport

      a pesar de que este grid-container tiene solo 4 grid-items, se crearán 4 columnas 
      como maximo, y mientras el viewport se reduce, el numero de columnas tambien, pero 
      ocuparan todo el viewport
    */
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr)); 
}
````


Si escribir grid-column y grid-row se te hace demasiado pesado, aquí tienes otra propiedad abreviada. grid-area admite cuatro valores separados por barras oblicuas: grid-row-start, grid-column-start, grid-row-end, seguido de grid-column-end.

Un ejemplo de esto podría ser grid-area: 1 / 1 / 3 / 6;

grid-area : 1 / 2 / 4 / 6;

los dos primeros son los comienzos de grid-lines de row y column respecivamente y los dos ultimos son los grid-lines finales de row y column respecivamente

otra solucion de la anterior grid-area : span 3 / 2 / auto / span 6 ;

Si los elementos de la cuadrícula no se sitúan explícitamente con grid-area, grid-column, grid-row, etc., se sitúan automáticamente de acuerdo al orden en el código fuente. Puedes sobrescribir esto usando la propiedad `order`, que es una de las ventajas de la cuadrícula frente al diseño basado en tablas.

Por defecto, el valor de order de todos los elementos es igual a 0, pero puede ser establecido a cualquier valor positivo o negativo, de manera similar a z-index.

grid-template es una propiedad abreviada que combina grid-template-rows y grid-template-columns.

Por ejemplo, grid-template: 50% 50% / 200px; creará una cuadrícula con dos filas que ocuparán el 50% del alto cada una, y una columna que será 200 píxeles de ancho.
ejemplo : grid-template : calc(100% - 50px) / 20% 80%;
grid-template: repeat(4,50px) / repeat(3,1fr);

fr > auto

- order default 0, igual que flexbox
- justify-items : start | end | center | stretch(default), parecido al justify-content de flexbox - Row axis
- align-items : start | end | center | stretch(default), parecido al align-items de flexbox - column axis
- justify-self : alinear un conjunto o un grid-item, mismos valores que los anteriores
- align-self : alinear un conjunto o un grid-item, mismos valores que los anteriores
