(tema flexbox) Valores predeterminados

- Si no se especifica una propiedad flex a un flex-item
  - por defecto es flex : 0 1 auto es decir que por defecto
    los flex-items no creceran pero si podran reducirse proporcionalmente, ademas el flex-basis en auto dirá que es respecto al contenido
- flex : auto , equivale a flex : 1 1 auto;
- flex : none; equivale a flex : 0 0 auto;
- flex : number , equivale a flex : number 0 0px;


Terminologia flexbox

- flex-container
- flex-item (hijos directos,textos,pseudoelementos)
- flex-lines : alinea los hijos en una solo linea (unidireccional) recordar que cada flex-line es independiente del resto
- main axis y cross axis
 - main size (main start,main end)
 - cross size (cross start,cross end)

Casos :

- Cuando el espacio disposible es positivo solo podrá aplicarse el flex-grow
- Cuando el espacio disponible es negativo solo podrá aplicarse el flex-shrink
- y tanto el flex-grow y flex-shrink dependen del flex-basis y este del main-size
https://developer.mozilla.org/es/docs/Web/CSS/Layout_mode
https://css-tricks.com/snippets/css/complete-guide-grid/
https://css-tricks.com/snippets/css/a-guide-to-flexbox/
http://slides.com/juanandresnunez/css-grid-lo-que-debes-saber#/
https://www.smashingmagazine.com/understanding-css-grid-template-areas/
https://ignaciodenuevo.com/speaking/css-grid-en-un-proyecto-real.html#slide=1
https://www.youtube.com/watch?v=RFQy-ud8Ec4&t=5208s
https://www.youtube.com/watch?v=ds2Ra3l3-OU

Aquí hay ejemplos míos
https://codepen.io/ThomMaurick

https://learncssgrid.com/

Mejor curso gratituo de Grid, siempre y cuando tengas experiencia en css
intermedio avanzado https://www.youtube.com/playlist?list=PLM-Y_YQmMEqBxmylkI5WJn9ouUxWlJNOW
